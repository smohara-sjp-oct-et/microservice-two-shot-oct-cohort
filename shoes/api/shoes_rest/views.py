from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "bin",
        "size",
        "import_href",
        ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin_closet": o.bin.name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = {}
        data = Shoe.objects.all()

        for shoe in data:
            bin = {
                "closet": shoe.bin.name,
                "bin": shoe.bin.bin,
                "size": shoe.bin.size,
                "id": shoe.bin.id,
            }
            model = {
                "model": shoe.model_name,
                "color": shoe.color,
                "picture": shoe.picture_url,
                "location": bin,
                "id": shoe.id,
            }
            print(model)
            if shoe.manufacturer in shoes:
                shoes[shoe.manufacturer].append(model)
            else:
                shoes[shoe.manufacturer] = [model]
            print(shoes)
        return JsonResponse(
            {"shoes": shoes},
            # encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin.  Use existing bin HREF"},
                status=400,
            )

        photo = get_photo(content["manufacturer"], content["model_name"])
        content.update(photo)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0})
    elif request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
