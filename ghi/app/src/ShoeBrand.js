import LocationFunction from "./ShoeLocation"


export default function BrandFunction(props) {
    async function ShoeDelete(shoeId) {
        const url = `http://localhost:8080/api/shoes/${shoeId}/`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const deleteConfirm = await response.json();
            props.fetchShoesData();
        }
    }



    return (
        <>
        <h2>{ props.brand }</h2>
        <table key={props.brand}className="table table-striped">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Model</th>
                <th>Color</th>
                <th>Location</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            {props.shoes.map(shoe => {
                    return (
                    <tr key={ shoe.id }>
                        <td style={{width: "10%"}}>
                            <img style={{width: "100%", aspectRatio: 1/1}} src={ shoe.picture } alt="" />
                        </td>
                        <td>{ shoe.model }</td>
                        <td>{ shoe.color }</td>
                            <LocationFunction location={shoe.location}></LocationFunction>
                        <td>
                            <button onClick={() => ShoeDelete(shoe.id)}>Delete</button>
                        </td>
                    </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}
