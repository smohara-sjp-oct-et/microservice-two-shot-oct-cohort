function LocationFunction(props) {
    const location = props.location;
    return (
        <td key={ location.id }>
            Closet: { location.closet }<br/>
            Bin: { location.bin }<br/>
            Size: { location.size }

        </td>
    )
}
export default LocationFunction
