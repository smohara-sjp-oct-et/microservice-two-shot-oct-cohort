import React, { useEffect, useState } from 'react'

function ShoeForm() {
    const [brands, setBrands] = useState([]);
    const [locations, setLocations] = useState([]);

    const fetchLocationData = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.bins);
        }
    }

    useEffect(() => {
        fetchLocationData();
    }, []);

    const [model, setModel] = useState('');

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value)
    }

    const [manufacturer, setManufacturer] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }

    const [color, setColor] = useState('');

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }

    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model;
        data.color = color;
        data.bin = location;

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();

            setManufacturer('');
            setModel('');
            setColor('');
            setLocation('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelChange} value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                <label htmlFor="Model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Where it be?</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name} - Bin:{location.bin_number}-{location.bin_size}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoeForm
