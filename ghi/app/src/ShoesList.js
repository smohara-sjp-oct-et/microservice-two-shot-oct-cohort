import { NavLink } from "react-router-dom";
import BrandFunction from "./ShoeBrand";
import LocationFunction from "./ShoeLocation";
import { useEffect, useState } from "react";

export default function ShoesList() {
    const [shoes, setShoes] = useState({})

    const fetchShoesData = async () => {
        const url = 'http://localhost:8080/api/shoes/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        fetchShoesData();
    }, []);



    return (
        <>
        <NavLink className="nav-link" to="/shoes/new">Add New Shoe</NavLink>
        {Object.entries(shoes).map(([brand, shoes]) => {
        return (
            <div key={brand}>
            <BrandFunction brand={brand} shoes={shoes} key={brand} ShoesList={ShoesList} fetchShoesData={fetchShoesData}/>
            </div>
        )
    })}
    </>
    )
}
