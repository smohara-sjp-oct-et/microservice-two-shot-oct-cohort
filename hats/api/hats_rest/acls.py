import json
import os

import requests

PEXELS_API_KEY = os.environ["PEXELS_API_KEY"]


def get_hat_photo(style_name, fabric, color):
    pass
    pexel_auth_header = {"Authorization": PEXELS_API_KEY}
    photo_search_params = {
        "query": f"{style_name}, {fabric}, {color}",
        "orientation": "square",
        "size": "medium",
        "per_page": 1,
    }

    url = "https://api.pexels.com/v1/search"
    photo_search_response = requests.get(
        url, params=photo_search_params, headers=pexel_auth_header
    )
    photo_search_info = json.loads(photo_search_response.content)

    try:
        return {"picture_url": photo_search_info["photos"][0]["src"]["medium"]}
    except (IndexError, KeyError):
        return {"picture_url": None}
