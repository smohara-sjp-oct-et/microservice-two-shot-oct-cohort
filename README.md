# Welcome to Wardrobify

### _Your one stop shop for keeping track on your hats and shoes_

## Features:
- Setup and edit your storage configuration
- Create Hats based on Fabric, Style and Color and assign them a location
- Create Shoes based on Manufacturer, Model and Color and assign them a location
- Frontend build with React
- RESTful API implementation for easy interaction
- Automatically adds stock photo based on your information

## Requirements:
- [git]
- [docker]

## Installation:

Fork repository and clone using git.
```bash
git clone https://gitlab.com/***YourNameHere***/microservice-two-shot-oct-cohort.git
```

Go to the directory that was just created
```bash
cd microservice-two-shot-oct-cohort
```

Create Docker volume, build and run your docker container
```bash
docker volume create two-shot-pgdata
docker-compose build
docker-compose up
```
Go to http://localhost:3000/ to access the home page.

## Wardrobe API Information
Wardrobe URL: [http://localhost:8100]
### Locations
| Method | URL | Behavior |
| ------ | ------ | ------ |
| GET | /api/locations/ | Gets a list of all of the locations |
| GET | /api/locations/<int:pk>/ | Gets the details of one location |
| POST | /api/locations/ | Creates a new location with the posted data |
| PUT | /api/locations/<int:pk>/ | Updates the details of one location |
| DELETE | /api/locations/<int:pk>/ | Deletes a single location |
Location creation requirements:
```
{
    "closet_name": "some_name"
    "section_number": "some_integer"
    "shelf_number": "some_integer"
}
```
### Bins
| Method | URL | Behavior |
| ------ | ------ | ------ |
| GET | /api/bins/ | Gets a list of all of the bins |
| GET | /api/bins/<int:pk>/ | Gets the details of one bin |
| POST | /api/bins/ | Creates a new bin with the posted data |
| PUT | /api/bins/<int:pk>/ | Updates the details of one bin |
| DELETE | /api/bins/<int:pk>/ | Deletes a single bin |
Bin creation requirements:
```
{
	"closet_name": "some_name",
	"bin_number": "some_integer",
	"bin_size": "some_integer"
}
```

## Acknowledgements
Team:

* Noah B. Bowman - Shoe Extraordinaire
* Sean O'Hara - Hats microservice

## Design



## Shoes microservice

- Created React frontend with list, creation and delete options.
- Created BinVO Value Object model to store location data in microservice, taking advantage of existing polling service to synchronize data.
- Created a dynamic back end RESTful API with a focus on usability and data organization.
- Integrated [pexels] API to grab stock photo data for shoe list frontend.

| Method | URL | Behavior |
| ------ | ------ | ------ |
| GET | /api/shoes/ | Returns list of shoes grouped in objects of manufacturers.  See example below. |
| POST | /api/shoes/ | Creates new shoe in existing location |
| DELETE | /api/shoes/<int:pk>/ | Deletes shoe by Primary Key and returns deleted response if successful |

Shoe list example:
```
{
	"shoes": {
		"Nike": [
			{
				"model": "Jordan",
				"color": "Blue",
				"picture": "https://images.pexels.com/photos/1031955/pexels-photo-1031955.jpeg",
				"location": {
					"closet": "Frank's Closet",
					"bin": 22,
					"size": 1,
					"id": 6
				},
				"id": 15
			}
		],
		"Converse": [
			{
				"model": "Chuck Taylor All Stars",
				"color": "Red",
				"picture": "https://images.pexels.com/photos/1477538/pexels-photo-1477538.jpeg",
				"location": {
					"closet": "Basement Closet",
					"bin": 1,
					"size": 4,
					"id": 1
				},
				"id": 20
			}
		]
	}
}
```

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

[//]: #
   [git]: <https://github.com/git-guides/install-git>
   [docker]: <https://docs.docker.com/get-docker/>
   [pexels]: <https://www.pexels.com/>
